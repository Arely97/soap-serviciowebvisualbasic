﻿Imports System.ComponentModel
Imports System.Web.Services
Imports System.Web.Services.Protocols

' Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://ServicioWebSoap2.somee.com")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class Suma
    Inherits System.Web.Services.WebService

    <WebMethod()>
    Public Function Add(intA As Double, intB As Double, intC As Double, intD As Double, intE As Double) As Double
        Dim suma = intA + intB + intC + intD + intE
        Return suma
    End Function
    <WebMethod()>
    Public Function promedio1(suma As Double) As Double
        Dim promedio = suma / 5
        Return promedio
    End Function
    <WebMethod()>
    Public Function Varianza(intA As Double, intB As Double, intC As Double, intD As Double, intE As Double, promedio As Double) As Double
        Dim cal1 = intA - promedio
        Dim cal2 = intB - promedio
        Dim cal3 = intC - promedio
        Dim cal4 = intD - promedio
        Dim cal5 = intE - promedio
        Dim sumita1 = cal1 + cal2 + cal3 + cal4 + cal5
        Dim cal11 = cal1 ^ 2
        Dim cal21 = cal2 ^ 2
        Dim cal31 = cal3 ^ 2
        Dim cal41 = cal4 ^ 2
        Dim cal51 = cal5 ^ 2
        Dim suma1 = cal11 + cal21 + cal31 + cal41 + cal51
        Dim varianza1 = suma1 / 4
        Return varianza1
    End Function
    <WebMethod()>
    Public Function desvia(varianza1 As Double) As Double
        Dim desviacion11 = Math.Sqrt(varianza1)
        Return desviacion11
    End Function

End Class